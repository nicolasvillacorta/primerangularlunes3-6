import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styles: []
})
export class BodyComponent implements OnInit {

  mostrar = true;
  frase: any = {
    autor: "Ben Parker",
    mensaje: "Un gran poder conlleva una gran responsabilidad"
  }

  personajes: string[] = ["Spiderman", "Superman", "Batman"]

  constructor() { }

  ngOnInit() {
  }

}
